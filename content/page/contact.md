+++
date = "2016-11-03T11:50:22-05:00"
title = "Contact Me"
url = "/contact/"
+++

Email: [camderwin@gmail.com](mailto:camderwin@gmail.com)

Here's my [LinkedIn], [Github], and [Gitlab].  Email first if you want my phone or address.

[LinkedIn]: https://linkedin.com/in/cderwin
[Github]: https://github.com/cderwin
[Gitlab]: https://gitlab.com/cderwin
