+++
title = "About Me"
date = "2016-11-03T02:06:41-05:00"
url = "/about/"
+++

Hi there!
My name is Cameron Derwin.
I am a (former) software engineer, (former) data scientist, and (current) amateur mathematician.
I currently study Mathematics and Computer Science at [The Univeristy of Texas at Dallas](http://iwww.utdallas.edu).


I primarily use Python, and do backend web development and machine learning.
I have experience using libraries like Django, Flask, Aiohttp, and pandas, nltk, and scikit-learn.
I often work with databases such as postgresql, redi, elasticsearch, and cassandra.
Often I utilize make, docker, and AWS to deploy web applications.


As a hobby, I am interested in some more technical fields:

* I enjoy programming languages.  Some of my favorites are rust, nim, and elm
* I am also interested in cryptography and computer security
* Math: I find type thoery and differential geometry particularly interesting.  But more than anything, I love to learn new math.


If you would like to contact me, please email me at [camderwin@gmail.com](mailto:camderwin@gmail.com).
My most up-to-date resume can be found on [LinkedIn], and most of my personal projects are hosted on [Github] or [Gitlab].

[LinkedIn]: https://linkedin.com/u/cderwin
[Github]: https://github.com/cderwin
[Gitlab]: https://gitlab.com/cderwin
